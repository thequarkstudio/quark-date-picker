### Description
<!-- Example: The `quark-date-picker` element does not open when the input is clicked. -->

### Expected outcome
<!-- Example: Overlay should appear when input is clicked. -->

### Actual outcome
<!-- Example: Overlay stays hidden. -->

### Live Demo
<!-- Example: https://jsbin.com/qojiwugino/edit?html,output -->

### Steps to reproduce
<!-- Example
1. Put a `quark-date-picker` element in the page.
2. Open the page in a web browser.
3. Click the input of `quark-date-picker` element.
-->

### Browsers Affected
<!-- Check all that apply -->
- [ ] Chrome
- [ ] Firefox
- [ ] Safari
- [ ] Edge
- [ ] IE 11
- [ ] iOS Safari
- [ ] Android Chrome
