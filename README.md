# &lt;quark-date-picker&gt;

[Live Demo ↗](https://cdn.quark.com/quark-core-elements/master/quark-date-picker/demo/)

[&lt;quark-date-picker&gt;](https://quark.com/elements/-/element/quark-date-picker) is a [Polymer](http://polymer-project.org) element providing a date selection field which includes a scrollable month calendar view, part of the [quark Core Elements](https://quark.com/elements).

<!--
```
<custom-element-demo height="550">
  <template>
    <link rel="import" href="quark-date-picker.html">
    <next-code-block></next-code-block>
  </template>
</custom-element-demo>
```
-->
```html
<quark-date-picker label="Birthday">
</quark-date-picker>
```

[<img src="https://raw.githubusercontent.com/quark/quark-date-picker/master/screenshot.png" width="439" alt="Screenshot of quark-date-picker">](https://quark.com/elements/-/element/quark-date-picker)


## Contributing

1. Fork the `quark-date-picker` repository and clone it locally.

1. Make sure you have [npm](https://www.npmjs.com/) installed.

1. When in the `quark-date-picker` directory, run `npm install` to install dependencies.


## Running demos and tests in browser

1. Install [polyserve](https://www.npmjs.com/package/polyserve): `npm install -g polyserve`

1. When in the `quark-date-picker` directory, run `polyserve --open`, browser will automatically open the component API documentation.

1. You can also open demo or in-browser tests by adding **demo** or **test** to the URL, for example:

  - http://127.0.0.1:8080/components/quark-date-picker/demo
  - http://127.0.0.1:8080/components/quark-date-picker/test


## Running tests from the command line

1. Install [web-component-tester](https://www.npmjs.com/package/web-component-tester): `npm install -g web-component-tester`

1. When in the `quark-date-picker` directory, run `wct` or `npm test`


## Following the coding style

We are using [ESLint](http://eslint.org/) for linting JavaScript code. You can check if your code is following our standards by running `gulp lint`, which will automatically lint all `.js` files as well as JavaScript snippets inside `.html` files.


## Creating a pull request

  - Make sure your code is compliant with our code linters: `gulp lint`
  - Check that tests are passing: `npm test`
  - [Submit a pull request](https://www.digitalocean.com/community/tutorials/how-to-create-a-pull-request-on-github) with detailed title and description
  - Wait for response from one of quark Elements team members


## License

Apache License 2.0
